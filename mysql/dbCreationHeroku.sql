CREATE TABLE persons_list (
  id SERIAL PRIMARY KEY,
  first_name CHAR(31) NOT NULL,
  last_name CHAR(31) NOT NULL,
  email CHAR(100) NOT NULL);
INSERT INTO persons_list (first_name, last_name, email) VALUES ( 'Igor', 'Pechkin', 'pechnik.igor@gmail.com');
INSERT INTO persons_list (first_name, last_name, email) VALUES ( 'Silver', 'Okorok', 'okorok.silver@gmail.com');
INSERT INTO persons_list (first_name, last_name, email) VALUES ( 'Karabas', 'Barabas', 'barabas.karabas@gmail.com');
INSERT INTO persons_list (first_name, last_name, email) VALUES ( 'Мадильда', 'Фрекенбок', 'frekenbok.matilda@gmail.com');