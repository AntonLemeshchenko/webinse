<?php

include_once ('../handlers/Database_pdo_heroku.php');
$db = new Database();
    //decode values for new user
    $data = json_decode(file_get_contents('php://input'), true);
    $result = $db->delete_row($data['id']);
    if($result)
    {
        $answer = json_encode(["answer"=>true]);
        echo $answer;
    }
    else
    {
        $answer = json_encode(["answer"=>false]);
        echo $answer;
    }