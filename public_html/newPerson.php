<?php
include_once ('../handlers/Database_pdo_heroku.php');
$db = new Database();
    //decode values for new user
    $data = json_decode(file_get_contents('php://input'), true);
    
    //crop the values
    $firstName = substr($data['firstName'], 0, 30);
    $lastName = substr($data['lastName'], 0, 30);
    $email = substr($data['email'], 0, 100);
    
    //validate values
    $firstName = filter_var($firstName, FILTER_SANITIZE_MAGIC_QUOTES);
    $lastName = filter_var($lastName, FILTER_SANITIZE_MAGIC_QUOTES);
    $email = filter_var($email, FILTER_VALIDATE_EMAIL);
    
    if($firstName && $lastName && $email)
    {
        $result = $db->insert_row($firstName, $lastName, $email);
        if($result)
        {
            $person = $db->get_row($result);
            if($person)
            {
                $answer = json_encode($person);
                echo $answer;
            }
            else
            {
                $answer = json_encode(["answer"=>"db_problem"]);
                echo $answer;
            }
        }
        else
        {
            $answer = json_encode(["answer"=>"db_problem"]);
            echo $answer;
        }
        
    }
    else
    {
        $answer = json_encode(["answer"=>"valid_problem"]);
        echo $answer;
    }