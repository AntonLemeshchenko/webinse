var searchParent = {
    // searching a parent element of "target" by RegExp object
    findParentByRegExp : function (target, regExp)
    {
        var targetParent = target;
        while (!(regExp.test(targetParent.classList)))
        {
            targetParent = targetParent.parentElement;
            if(targetParent==null)
            {
                return false;
            }
        }
        return targetParent;
    }
}