var errorMessage = {

    customReporValidity : function (element, message)
    {
        element.setCustomValidity("");
        if(!element.reportValidity())
        {
            element.setCustomValidity(message);
            return false;   
        }
        return true;
    }
}