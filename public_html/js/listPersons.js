
var listPersons =
{
    //creating new person and Validation on click 
    createNewPerson: function () {
        //main variables
        var listFramePersons = document.querySelector('.list-frame-persons');
        var createInputFirstName = document.querySelector('.list-frame-create-first-name');
        var createInputLastName = document.querySelector('.list-frame-create-last-name');
        var createInputEmail = document.querySelector('.list-frame-create-email');
        var personExample = document.querySelector('.person-example');

        if (!errorMessage.customReporValidity(createInputFirstName, "Person first name is empty or too long!")) {
            return false;
        }
        else if (!errorMessage.customReporValidity(createInputLastName, "Person last name is empty or too long!")) {
            return false;
        }
        else if (!errorMessage.customReporValidity(createInputEmail, "Person email is not correct! Ex.: abc@def.ghi")) {
            return false;
        }
        else {
            //formation of data for server
            var data =
            {
                firstName: createInputFirstName.value,
                lastName: createInputLastName.value,
                email: createInputEmail.value,
            }

            fetch('/newPerson.php',
                {
                    method: 'POST',
                    body: JSON.stringify(data),
                }).then(function (response) {
                    //return response.text();
                    return response.json();
                }).then(function (dataResp) {
                    //console.log(dataResp);
                    if (!dataResp.answer) {
                        //Cloning a new node on base of example, inserting new name and adding it to our project
                        var newNode = personExample.cloneNode(true);
                        newNode.classList.remove("person-example");
                        newNode.id = dataResp['id'];
                        newNode.querySelector(".list-frame-persons-person-first-name").textContent = dataResp['first_name'];
                        newNode.querySelector(".list-frame-persons-person-last-name").textContent = dataResp['last_name'];
                        newNode.querySelector(".list-frame-persons-person-email").textContent = dataResp['email'];
                        listFramePersons.appendChild(newNode);
                        createInputFirstName.value = "";
                        createInputLastName.value = "";
                        createInputEmail.value = "";
                        return true;
                    }
                    else if (dataResp.answer) {
                        createInputFirstName.value = "";
                        createInputLastName.value = "";
                        createInputEmail.value = "";
                        message = "Sorry, but your data cannot be saved now. ";
                        message += dataResp.answer == "db_problem" ? "DB is occupied. Please try again later" : "Data is invalid. Please verify your data and try again.";
                        alert(message);
                        return false;
                        //console.log(dataResp);
                        //console.log(dataResp.answer);
                    }

                });

            return false;
        }
    },

    //starting changing the name of existed task (appearence of input-field and checking value on input)
    editPerson: function (targetPerson, action) {
        //main variables
        var personFirstName = targetPerson.querySelector(".list-frame-persons-person-first-name");
        var personLastName = targetPerson.querySelector(".list-frame-persons-person-last-name");
        var personEmail = targetPerson.querySelector(".list-frame-persons-person-email");

        var personFirstNameInput = targetPerson.querySelector(".list-frame-persons-person-first-name-change");
        var personLastNameInput = targetPerson.querySelector(".list-frame-persons-person-last-name-change");
        var personEmailInput = targetPerson.querySelector(".list-frame-persons-person-email-change");

        var personFirstNameOld = personFirstName.innerHTML.replace(/\s{2,}/g, "");
        var personLastNameOld = personLastName.innerHTML.replace(/\s{2,}/g, "");
        var personEmailOld = personEmail.innerHTML.replace(/\s{2,}/g, "");

        //starting to change person data
        if (action == "start") {
            //changing table text to input for editing
            personFirstName.classList.add('d-none');
            personLastName.classList.add('d-none');
            personEmail.classList.add('d-none');
            personFirstName.classList.remove('d-flex');
            personLastName.classList.remove('d-flex');
            personEmail.classList.remove('d-flex');

            personFirstNameInput.classList.remove('d-none');
            personLastNameInput.classList.remove('d-none');
            personEmailInput.classList.remove('d-none');
            personFirstNameInput.classList.add('d-flex');
            personLastNameInput.classList.add('d-flex');
            personEmailInput.classList.add('d-flex');

            //changing the icon for editing
            targetPerson.querySelector('.fa-floppy-o').classList.remove('d-none');
            targetPerson.querySelector('.fa-pencil-square-o').classList.add('d-none');
            targetPerson.querySelector('.fa-ban').classList.remove('d-none');
            targetPerson.querySelector('.fa-times').classList.add('d-none');

            //entering the old value in input
            personFirstNameInput.querySelector("input").value = personFirstNameOld;
            personLastNameInput.querySelector("input").value = personLastNameOld;
            personEmailInput.querySelector("input").value = personEmailOld;

            personFirstNameInput.querySelector("input").select();
            return true;
        }
        //finishing to change person data and save
        else if (action == "end") {
            if (!errorMessage.customReporValidity(personFirstNameInput.querySelector("input"), "Person first name is empty or too long!")) {
                return false;
            }
            else if (!errorMessage.customReporValidity(personLastNameInput.querySelector("input"), "Person last name is empty or too long!")) {
                return false;
            }
            else if (!errorMessage.customReporValidity(personEmailInput.querySelector("input"), "Person email is not correct! Ex.: abc@def.ghi")) {
                return false;
            }
            else {
                data =
                    {
                        id: targetPerson.id,
                        firstName: personFirstNameInput.querySelector("input").value,
                        lastName: personLastNameInput.querySelector("input").value,
                        email: personEmailInput.querySelector("input").value,
                    };
                fetch('/changePerson.php',
                    {
                        method: 'POST',
                        body: JSON.stringify(data),
                    }).then(function (response) {
                        //return response.text();
                        return response.json();
                    }).then(function (dataResp) {
                        //console.log(dataResp);
                        if (dataResp.answer == "db_problem" || dataResp.answer == "valid_problem") {
                            //changing input to table text
                            personFirstName.classList.add('d-flex');
                            personLastName.classList.add('d-flex');
                            personEmail.classList.add('d-flex');
                            personFirstName.classList.remove('d-none');
                            personLastName.classList.remove('d-none');
                            personEmail.classList.remove('d-none');

                            personFirstNameInput.classList.remove('d-flex');
                            personLastNameInput.classList.remove('d-flex');
                            personEmailInput.classList.remove('d-flex');
                            personFirstNameInput.classList.add('d-none');
                            personLastNameInput.classList.add('d-none');
                            personEmailInput.classList.add('d-none');

                            //changing the icon for editing
                            targetPerson.querySelector('.fa-floppy-o').classList.add('d-none');
                            targetPerson.querySelector('.fa-pencil-square-o').classList.remove('d-none');
                            targetPerson.querySelector('.fa-ban').classList.add('d-none');
                            targetPerson.querySelector('.fa-times').classList.remove('d-none');

                            message = "Sorry, but your data cannot be saved. ";
                            message += dataResp.answer == "db_problem" ? "DB is occupied. Please try again later" : "Data is invalid. Please verify your data and try again.";
                            alert(message);
                            return false;
                        }
                        else if (dataResp.id>0) {
                            //changing input to table text
                            personFirstName.classList.add('d-flex');
                            personLastName.classList.add('d-flex');
                            personEmail.classList.add('d-flex');
                            personFirstName.classList.remove('d-none');
                            personLastName.classList.remove('d-none');
                            personEmail.classList.remove('d-none');

                            personFirstNameInput.classList.remove('d-flex');
                            personLastNameInput.classList.remove('d-flex');
                            personEmailInput.classList.remove('d-flex');
                            personFirstNameInput.classList.add('d-none');
                            personLastNameInput.classList.add('d-none');
                            personEmailInput.classList.add('d-none');

                            //changing the icon for editing
                            targetPerson.querySelector('.fa-floppy-o').classList.add('d-none');
                            targetPerson.querySelector('.fa-pencil-square-o').classList.remove('d-none');
                            targetPerson.querySelector('.fa-ban').classList.add('d-none');
                            targetPerson.querySelector('.fa-times').classList.remove('d-none');

                            //entering the new value from input in table
                            personFirstName.textContent = dataResp['first_name'];
                            personLastName.textContent = dataResp['last_name'];
                            personEmail.textContent = dataResp['email'];

                            return true;
                        }
                        else {
                            //changing input to table text
                            personFirstName.classList.add('d-flex');
                            personLastName.classList.add('d-flex');
                            personEmail.classList.add('d-flex');
                            personFirstName.classList.remove('d-none');
                            personLastName.classList.remove('d-none');
                            personEmail.classList.remove('d-none');

                            personFirstNameInput.classList.remove('d-flex');
                            personLastNameInput.classList.remove('d-flex');
                            personEmailInput.classList.remove('d-flex');
                            personFirstNameInput.classList.add('d-none');
                            personLastNameInput.classList.add('d-none');
                            personEmailInput.classList.add('d-none');

                            //changing the icon for editing
                            targetPerson.querySelector('.fa-floppy-o').classList.add('d-none');
                            targetPerson.querySelector('.fa-pencil-square-o').classList.remove('d-none');
                            targetPerson.querySelector('.fa-ban').classList.add('d-none');
                            targetPerson.querySelector('.fa-times').classList.remove('d-none');
                            return false;
                        }

                    });

            }
        }
        //finishing to change person data without saving
        else if (action == "cancel") {
            //changing input to table text
            personFirstName.classList.add('d-flex');
            personLastName.classList.add('d-flex');
            personEmail.classList.add('d-flex');
            personFirstName.classList.remove('d-none');
            personLastName.classList.remove('d-none');
            personEmail.classList.remove('d-none');

            personFirstNameInput.classList.remove('d-flex');
            personLastNameInput.classList.remove('d-flex');
            personEmailInput.classList.remove('d-flex');
            personFirstNameInput.classList.add('d-none');
            personLastNameInput.classList.add('d-none');
            personEmailInput.classList.add('d-none');

            //changing the icon for editing
            targetPerson.querySelector('.fa-floppy-o').classList.add('d-none');
            targetPerson.querySelector('.fa-pencil-square-o').classList.remove('d-none');
            targetPerson.querySelector('.fa-ban').classList.add('d-none');
            targetPerson.querySelector('.fa-times').classList.remove('d-none');

            //entering the old value in table
            personFirstName.textContent = personFirstNameOld;
            personLastName.textContent = personLastNameOld;
            personEmail.textContent = personEmailOld;

            //далее отправка данных на сервер (или перед этим!)
            return true;
        }
        else {
            return false;
        }


    },

    deletePerson: function (targetPerson) {
        data = { id: targetPerson.id };
        fetch('/deletePerson.php',
            {
                method: 'POST',
                body: JSON.stringify(data),
            }).then(function (response) {
                //return response.text();
                return response.json();
            }).then(function (dataResp) {
                //console.log(dataResp);
                if (dataResp.answer) {
                    targetPerson.remove();
                    return true;
                }
                else if (!dataResp.answer) {
                    message = "Sorry. DB is occupied. Please try again later";
                    alert(message);
                    return false;
                    //console.log(dataResp);
                    //console.log(dataResp.answer);
                }

            });
        return false;
    },

    //initialization function
    initialize: function () {
        //handlers for click
        document.querySelector(".list").onclick = function (e) {

            //create new person
            if (/list-frame-create-apply/.test(e.target.className)) {
                listPersons.createNewPerson();
            }

            //change person data
            else if (/fa-pencil-square-o/.test(e.target.className)) {
                //var targetPerson = e.target.parentElement.parentElement;
                var targetPerson = searchParent.findParentByRegExp(e.target, /list-frame-persons-person /);
                listPersons.editPerson(targetPerson, "start");
            }

            //save edited person data
            else if (/fa-floppy-o/.test(e.target.className)) {
                var targetPerson = searchParent.findParentByRegExp(e.target, /list-frame-persons-person /);
                listPersons.editPerson(targetPerson, "end");
            }

            //cancel editing person data
            else if (/fa-ban/.test(e.target.className)) {
                var targetPerson = searchParent.findParentByRegExp(e.target, /list-frame-persons-person /);
                listPersons.editPerson(targetPerson, "cancel");
            }

            // delete person
            else if (/fa-times/.test(e.target.className)) {
                var targetPerson = searchParent.findParentByRegExp(e.target, /list-frame-persons-person /);
                listPersons.deletePerson(targetPerson);
            }

            //another click
            else {
                return false;
            }

        }

        //handlers for input events
        document.querySelector(".list").oninput = function (e) {
            //clear verification message on input until clicking on "Add"
            if (/list-frame-create-first-name/.test(e.target.className)) 
            {
                e.target.setCustomValidity("");
                return true;
            }
            else if (/list-frame-create-last-name/.test(e.target.className)) 
            {
                e.target.setCustomValidity("");
                return true;
            }
            else if (/list-frame-create-email/.test(e.target.className)) 
            {
                e.target.setCustomValidity("");
                return true;
            }
            else if (/list-frame-persons-person-first-name-change/.test(e.target.parentElement.classList)) 
            {
                e.target.setCustomValidity("");
                return true;
            }
            else if (/list-frame-persons-person-last-name-change/.test(e.target.parentElement.classList)) 
            {
                e.target.setCustomValidity("");
                return true;
            }
            else if (/list-frame-persons-person-email-change/.test(e.target.parentElement.classList)) 
            {
                e.target.setCustomValidity("");
                return true;
            }
            else 
            {
                return false;
            }
        }
    },

};

//initialization
listPersons.initialize();