<?php

include_once ('../handlers/Database_pdo_heroku.php');
$db = new Database();
    //decode values for new user
    $data = json_decode(file_get_contents('php://input'), true);
    
    //crop the values
    $newFirstName = substr($data['firstName'], 0, 30);
    $newLastName = substr($data['lastName'], 0, 30);
    $newEmail = substr($data['email'], 0, 100);
    
    //validate values
    $id = filter_var($data['id'], FILTER_VALIDATE_INT);
    $newFirstName = filter_var($newFirstName, FILTER_SANITIZE_MAGIC_QUOTES);
    $newLastName = filter_var($newLastName, FILTER_SANITIZE_MAGIC_QUOTES);
    $newEmail = filter_var($newEmail, FILTER_VALIDATE_EMAIL);
    
    if($id && $newFirstName && $newLastName && $newEmail)
    {
        $result = $db->change_row($id, $newFirstName, $newLastName, $newEmail);
        if($result)
        {
            $changedPerson = $db->get_row($id);
            $answer = json_encode($changedPerson);
            echo $answer;
        }
        else
        {
            $answer = json_encode(["answer"=>"db_problem"]);
            echo $answer;
        }
        
    }
    else
    {
        $answer = json_encode(["answer"=>"valid_problem"]);
        echo $answer;
    }