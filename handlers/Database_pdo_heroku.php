<?php

class Database
{
    private $host = "ec2-23-21-115-109.compute-1.amazonaws.com";
    private $username = "qnxajeqiislyth";
    private $password = "bc68e4d83deaaf720defaadebba0d24e80dcbac540091d190a74f969ab337125";
    private $database = "dcb8k5b0hg2890";
    private $table = 'persons_list';
    private $port = "5432";
    
    private function db_connect()
    {
        $conn = new PDO("pgsql:host=$this->host;port=$this->port;dbname=$this->database;user=$this->username;password=$this->password");
        if ($conn->errorCode()!='00000')
        {
            return $conn;
        }
        else
        {
            return false;
        }
    }
    
    function insert_row ($firstName, $lastName, $email)
    {
        $conn = $this->db_connect();
        if($conn===false)
        {
            return false;
        }
        $query = "INSERT INTO ".$this->table." (first_name, last_name, email) VALUES ('".$firstName."', '".$lastName."', '".$email."')";
        $result = $conn->query($query);
        if($result)
        {
            if($result->errorCode()=='00000')
            {
                $id = $conn->lastInsertId();
                //var_dump($id);
                $result=NULL;
                $conn=NULL;
                return $id;
            }
            else
            {
                $result=NULL;
                $conn=NULL;
                return false;
            }
        }
        else
        {
            $result=NULL;
            $conn=NULL;
            return false;
        }
        
    }
    
    function get_row ($id)
    {
        $conn = $this->db_connect();
        if($conn===false)
        {
            return false;
        }
        $query = "SELECT * FROM ".$this->table." WHERE id='".$id."'";
        $result = $conn->query($query);
        if(!$result)
        {
            $result=NULL;
            $conn = NULL;
            return false;
        }
        if($result->errorCode()=='00000')
        {
            if($result->rowCount()>0)
            {
                $row = $result->fetch(PDO::FETCH_BOTH);
                $result=NULL;
                $conn=NULL;
                return $row;
            }
            else
            {
                $result=NULL;
                $conn=NULL;
                return false;
            }
        }
        else
        {
            $result=NULL;
            $conn=NULL;
            return false;
        }
    }
    
    function get_all_rows()
    {
        $conn = $this->db_connect();
        if($conn === false)
        {
            return false;
        }
        
        $query = 'SELECT * FROM '.$this->table;
        $result = $conn->query($query);
        if(!$result)
        {
            $result=NULL;
            $conn=NULL;
            return false;
        }
        if($result->errorCode()=='00000')
        {
            if($result->rowCount()>0)
            {
                $rows = array();
                for($i=0; $i < $result->rowCount(); $i++)
                {
                    $rows[$i] =$result->fetch(PDO::FETCH_BOTH);
                }
                $result=NULL;
                $conn=NULL;
                return $rows;
            }
            else
            {
                $result=NULL;
                $conn=NULL;
                return false;
            }
        }
        else
        {
            $result=NULL;
            $conn=NULL;
            return false;
        }
    }
    
    function delete_row ($id)
    {
        $conn = $this->db_connect();
        if($conn===false)
        {
            return false;
        }
        $query = "DELETE FROM ".$this->table." WHERE id='".$id."'";
        $result = $conn->query($query);
        if($result)
        {
            $result=NULL;
            $conn=NULL;
            return true;
        }
        else
        {
            $result=NULL;
            $conn=NULL;
            return false;
        }
    }
    
    function change_row ($id, $newFirstName, $newLastName, $newEmail)
    {
        $conn = $this->db_connect();
        if($conn===false)
        {
            return false;
        }
        $query = "UPDATE ".$this->table." SET first_name='".$newFirstName."', last_name='".$newLastName."', email='".$newEmail."' WHERE id='".$id."'";
        $result = $conn->query($query);
        if($result)
        {
            if($result->errorCode()=='00000')
            {
                $result=NULL;
                $conn=NULL;
                return true;
            }
            else
            {
                $result=NULL;
                $conn=NULL;
                return false;
            }
        }
        else
        {
            $result=NULL;
            $conn=NULL;
            return false;
        }
    }
}

