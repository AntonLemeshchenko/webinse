<?php
    $path = filter_input(INPUT_SERVER, 'REQUEST_URI');
    if($path==="/")
    {
        $route="/";
        $title = "E.Y.E";
    }
    else if ($path=="/newPerson")
    {
        $route="newPerson";
    }
    else if ($path=="/changePerson")
    {
        $route="changePerson";
    }
    else if ($path=="/deletePerson")
    {
        $route="deletePerson";
    }
    else
    {
        $route="Error404";
        $title = "Page not found";
    }
?>