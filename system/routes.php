<?php

//Изначально не хотел создавать отдельные страницы newPerson.php, changePerson.php и deletePerson.php
// но после 4 часов надежда найти, как перенаправить все запросы в herocu Apache2 (хотя на локальном Apache2 все работает)
// на index.php, пропала :(
if(filter_input(INPUT_SERVER, 'REQUEST_METHOD')!='POST')
{
    include '../pages/header.html';
}

if($route == "/")
{
    $rows = $db->get_all_rows();
    include '../pages/home.html';
}
else if($route == "Error404")
{
    include '../pages/error404.html';
}
else if ($route=="newPerson")
{
    //decode values for new user
    $data = json_decode(file_get_contents('php://input'), true);
    
    //crop the values
    $firstName = substr($data['firstName'], 0, 30);
    $lastName = substr($data['lastName'], 0, 30);
    $email = substr($data['email'], 0, 100);
    
    //validate values
    $firstName = filter_var($firstName, FILTER_SANITIZE_MAGIC_QUOTES);
    $lastName = filter_var($lastName, FILTER_SANITIZE_MAGIC_QUOTES);
    $email = filter_var($email, FILTER_VALIDATE_EMAIL);
    
    if($firstName && $lastName && $email)
    {
        $result = $db->insert_row($firstName, $lastName, $email);
        if($result)
        {
            $person = $db->get_row($result);
            if($person)
            {
                $answer = json_encode($person);
                echo $answer;
            }
            else
            {
                $answer = json_encode(["answer"=>"db_problem"]);
                echo $answer;
            }
        }
        else
        {
            $answer = json_encode(["answer"=>"db_problem"]);
            echo $answer;
        }
        
    }
    else
    {
        $answer = json_encode(["answer"=>"valid_problem"]);
        echo $answer;
    }
        
    
}
else if($route=="changePerson")
{
    //decode values for new user
    $data = json_decode(file_get_contents('php://input'), true);
    
    //crop the values
    $newFirstName = substr($data['firstName'], 0, 30);
    $newLastName = substr($data['lastName'], 0, 30);
    $newEmail = substr($data['email'], 0, 100);
    
    //validate values
    $id = filter_var($data['id'], FILTER_VALIDATE_INT);
    $newFirstName = filter_var($newFirstName, FILTER_SANITIZE_MAGIC_QUOTES);
    $newLastName = filter_var($newLastName, FILTER_SANITIZE_MAGIC_QUOTES);
    $newEmail = filter_var($newEmail, FILTER_VALIDATE_EMAIL);
    
    if($id && $newFirstName && $newLastName && $newEmail)
    {
        $result = $db->change_row($id, $newFirstName, $newLastName, $newEmail);
        if($result)
        {
            $changedPerson = $db->get_row($id);
            $answer = json_encode($changedPerson);
            echo $answer;
        }
        else
        {
            $answer = json_encode(["answer"=>"db_problem"]);
            echo $answer;
        }
        
    }
    else
    {
        $answer = json_encode(["answer"=>"valid_problem"]);
        echo $answer;
    }
}
else if($route=="deletePerson")
{
    //decode values for new user
    $data = json_decode(file_get_contents('php://input'), true);
    $result = $db->delete_row($data['id']);
    if($result)
    {
        $answer = json_encode(["answer"=>true]);
        echo $answer;
    }
    else
    {
        $answer = json_encode(["answer"=>false]);
        echo $answer;
    }
}

if(filter_input(INPUT_SERVER, 'REQUEST_METHOD')!='POST')
{
    include '../pages/footer.html';
}
?>